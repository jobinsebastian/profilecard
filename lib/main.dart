import 'package:containerdemo/myContainer.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

//create a class named MyApp

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      home: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment:MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:const [
              MyContainer(),
            ],
          ),
        ),
      ),
    );
  }
}
