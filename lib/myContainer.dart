import 'package:flutter/material.dart';

class MyContainer extends StatelessWidget {
  const MyContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      width: 350,
      child: Stack(
        children: [
          Center(
            child: Container(
              height: 180,
              width: 350,
              decoration: BoxDecoration(
                gradient:
                    const LinearGradient(colors: [Colors.teal, Colors.purple]),
                borderRadius: BorderRadius.circular(18),
              ),
            ),
          ),

          //use the positioned widget to place

          Positioned(
            top: 0,
            right: 40,
            child: Container(
              height: 80,
              width: 80,
              decoration: const BoxDecoration(
                  color: Colors.green, shape: BoxShape.circle),
              child: const CircleAvatar(
                radius: 30,
              ),
            ),
          ),

          const Positioned(
            top: 65,
            left: 40,
            child: Text(
              "Mary Jane",
              style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700),
            ),
          ),

          const Positioned(
            top: 100,
            left: 40,
            child: Text(
              "Assitant Professor",
              style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 16.0,
                  fontWeight: FontWeight.w500),
            ),
          ),

          const Positioned(
            top: 120,
            left: 40,
            child: Text(
              "Department of Computer Science",
              style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 16.0,
                  fontWeight: FontWeight.w500),
            ),
          ),

          Positioned(
            bottom: 55,
            left: 20,
            child: SizedBox(
              width: 300,
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Icon(Icons.phone),
                    Text(
                      "+91 9048904851",
                      style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 14.0,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
                const SizedBox(
                  width: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Icon(Icons.email),
                    Text(
                      "+91 9048904851",
                      style: TextStyle(
                          color: Color.fromARGB(255, 255, 255, 255),
                          fontSize: 14.0,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              ]),
            ),
          ),

          const Positioned(
            right: 40,
            top: 85,
            child: Center(
              child: Text(
                "View Profile",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
